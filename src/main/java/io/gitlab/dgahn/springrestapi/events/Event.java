package io.gitlab.dgahn.springrestapi.events;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@EqualsAndHashCode(of = "id")
// Equals와 HashCode를 구현할 때 기본적으로 모든 필드를 사용하는 엔티티간의 연관관계가 있을 때, 연관관계가 상호 참조하는 경우 Equals와 HashCode 구현한 코드 안에서 스택오버플로우가 발생할 수 있다. 그래서 "id" 값으로 비교하도록 해주는 기능이다. 꼭 하나의 필드로 비교할 필요는 없다. 단, 연관관계에 관련된 필드를 넣어주면 안된다.
public class Event {

  @Id
  @GeneratedValue
  private Integer id;
  private String name;
  private String description;
  private LocalDateTime beginEnrollmentDateTime;
  private LocalDateTime closeEnrollmentDateTime;
  private LocalDateTime beginEventDateTime;
  private LocalDateTime endEventDateTime;
  private String location; // (optional)
  private int basePrice; // (optional)
  private int maxPrice; // (optional)
  private int limitOfEnrollment;
  private boolean offline;
  private boolean free;
  @Enumerated(EnumType.STRING) // Enum 타입을 스트링으로 사용할 수 있게 한다.
  private EventStatus eventStatus = EventStatus.DRAFT;

  public void update() {
    // Update free
    if (this.basePrice == 0 && this.maxPrice == 0) {
      this.free = true;
    } else {
      this.free = false;
    }

    //update offline
    if (this.location == null || this.location.isEmpty() || this.location.trim().isEmpty()) {
      this.offline = false;
    } else {
      this.offline = true;
    }
  }
}
