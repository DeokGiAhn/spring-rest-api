package io.gitlab.dgahn.springrestapi.events;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;


// 빈이 아니라 객체를 생성해서 사용해야 한다.
public class EventResource extends Resource<Event> { // 명시적으로 언랩을 하지 않아도 알아서 언랩을 한다.

  public EventResource(Event event, Link... links) {
    super(event, links);
    add(linkTo(EventController.class).slash(event.getId()).withSelfRel());
  }

// ResourceSupport를 상속 받아서 구현할 경우
//  @JsonUnwrapped
//  private Event event;
//
//  public EventResource(Event event) {
//    this.event = event;
//  }
//
//  public Event getEvent() {
//    return event;
//  }
}
