package io.gitlab.dgahn.springrestapi.events;

public enum EventStatus {

  DRAFT, PUBLISHED, BEGAN_ENROLLMENT;
}
