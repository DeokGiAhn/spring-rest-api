package io.gitlab.dgahn.springrestapi.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE) // 애노테이션을 붙인 코드를 언제까지 붙일 것인가에 대한 내용
public @interface TestDescription {

  String value();

}
